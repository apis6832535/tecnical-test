<?php

namespace App\Http\Controllers;

use App\Models\Character;
use Illuminate\Http\Request;

class CharacterController extends Controller
{
    /**
     * Display a listing of the characters.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $characters = Character::all();
        return response()->json($characters);
    }

    /**
     * Display the specified character.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $character = Character::findOrFail($id);
        return response()->json($character);
    }
    
    /**
     * Store a newly created character in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $character = Character::create($request->all());
        return response()->json($character, 201);
    }

    /**
     * Update the specified character in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $character = Character::findOrFail($id);

        // Validar si el modelo existe
        if ($character) {
            $character->update($request->all());
            return response()->json($character, 200);
        } else {
            return response()->json(["message" => "Character not found"], 404);
        }
    }

    /**
     * Remove the specified character from the database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $result = Character::destroy($id);
        
        // Verificar si la eliminación tuvo éxito
        if ($result) {
            return response()->json(["message" => "Character deleted"], 200);
        } else {
            return response()->json(["message" => "Character not found"], 404);
        }
    }
}
