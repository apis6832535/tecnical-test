<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use Illuminate\Http\Request;

class EpisodeController extends Controller
{
    /**
     * Display a listing of the episodes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $episodes = Episode::all();
        return response()->json($episodes);
    }

    /**
     * Display the specified episode.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $episode = Episode::findOrFail($id);
        return response()->json($episode);
    }

    /**
     * Store a newly created episode in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $episode = Episode::create($request->all());
        return response()->json($episode, 201);
    }

    /**
     * Update the specified episode in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $episode = Episode::find($id);

        // Validate if the model exists
        if ($episode) {
            $episode->update($request->all());
            return response()->json($episode, 200);
        } else {
            return response()->json(["message" => "Episode not found"], 404);
        }
    }

    /**
     * Remove the specified episode from the database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $result = Episode::destroy($id);

        // Check if the deletion was successful
        if ($result) {
            return response()->json(["message" => "Episode deleted"], 200);
        } else {
            return response()->json(["message" => "Episode not found"], 404);
        }
    }
}
