<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    use HasFactory;

    // Nombre de la tabla en la base de datos
    protected $table = 'characters';

    // Campos que se pueden llenar (en masa) en la creación o actualización del modelo
    protected $fillable = [
        'name', 'status', 'species', 'type', 'gender', 'origin', 'location', 'image', 'episode', 'url', 'created'
    ];

    // Deshabilitar la inclusión de marcas de tiempo (timestamps)
    public $timestamps = false;
}
