<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    use HasFactory;

    // Nombre de la tabla en la base de datos
    protected $table = 'episodes';

    // Campos que se pueden llenar (en masa) en la creación o actualización del modelo
    protected $fillable = [
        'name', 'air_date', 'episode', 'characters', 'url', 'created', 'starring'
    ];

    // Deshabilitar la inclusión de marcas de tiempo (timestamps)
    public $timestamps = false;
}
