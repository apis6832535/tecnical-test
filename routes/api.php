<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CharacterController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\EpisodeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Aquí es donde puedes registrar las rutas API para tu aplicación.
| Estas rutas son cargadas por RouteServiceProvider y todas ellas
| serán asignadas al grupo de middleware "api". ¡Crea algo genial!
|
*/

// Grupo de rutas para el controlador de personajes
Route::group([
    'prefix' => '/characters',
    'as' => 'characters',
], function () {
    Route::get('', [CharacterController::class, 'index']);
    Route::get('/{id}', [CharacterController::class, 'show']);
    Route::post('', [CharacterController::class, 'store']);
    Route::put('/{id}', [CharacterController::class, 'update']);
    Route::delete('/{id}', [CharacterController::class, 'destroy']);
});

// Grupo de rutas para el controlador de ubicaciones
Route::group([
    'prefix' => '/locations',
    'as' => 'locations',
], function () {
    Route::get('', [LocationController::class, 'index']);
    Route::get('/{id}', [LocationController::class, 'show']);
    Route::post('', [LocationController::class, 'store']);
    Route::put('/{id}', [LocationController::class, 'update']);
    Route::delete('/{id}', [LocationController::class, 'destroy']);
});

// Grupo de rutas para el controlador de episodios
Route::group([
    'prefix' => '/episodes',
    'as' => 'episodes',
], function () {
    Route::get('', [EpisodeController::class, 'index']);
    Route::get('/{id}', [EpisodeController::class, 'show']);
    Route::post('', [EpisodeController::class, 'store']);
    Route::put('/{id}', [EpisodeController::class, 'update']);
    Route::delete('/{id}', [EpisodeController::class, 'destroy']);
});
